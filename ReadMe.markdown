Kaeforest
=================

Don't use this template or code.
--------------------------------

We're not saying this to be mean or because our code is ~amazing~ **(protip: it isn't)** -
and because of this, we're warning you to avoid the headache.
Everything in this repo only makes sense to us, in the true spirit of open source.

We do not care there is *no* documentation - we simply wish to change CSS on occasion & write posts - have the site maintain itself with minimal interaction.
*And this is what it does.*

If you really want to have a site, make your own; go learn [11ty](<https://11ty.dev>) like we have & make something better than this.

Custom implemented features
-------------------------

+ Foam-based wiki-links system
+ Fully working system of backlinks
+ Drafting of posts using `.11tydata.js`
+ Optimised images, with the `{% image %}` shortcode using `@11ty/image`
+ Adding to the navbar by adding the *`nav`* tag.
+ All styling done through **stylus** with a custom compilation process
+ Full syntax highlighting using prism.js vendor css in the `/stylus/vendor` directory
+ Mathematical latex formatting using *Mathjax*
+ Multiple collections separated by their tags.
  + Nav
  + Daily-Notes
  + Audio
  + Dev
  + Art
