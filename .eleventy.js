import { DateTime as DT } from "luxon"
import syntaxHighlight from "@11ty/eleventy-plugin-syntaxhighlight"
import pluginRss from "@11ty/eleventy-plugin-rss"
import directoryOutputPlugin from "@11ty/eleventy-plugin-directory-output"
import eleventyNavigationPlugin from "@11ty/eleventy-navigation"
import {EleventyRenderPlugin, EleventyI18nPlugin, EleventyHtmlBasePlugin} from "@11ty/eleventy"
import {eleventyImageTransformPlugin}  from "@11ty/eleventy-img"
import mathjax3 from "markdown-it-mathjax3";
import markdownIt from "markdown-it";
import markdownItEmoji from "markdown-it-emoji";
import markdownItAnchor from "markdown-it-anchor";
import markdownItMath from "markdown-it-math";
import markdownItUnderline from "markdown-it-ins";
import markdownItReplaceLink from "markdown-it-replace-link";
import pluginTOC from "eleventy-plugin-toc";
import markdownItFootnote from "markdown-it-footnote";
import fs from "node:fs";
import embedYouTube from "eleventy-plugin-youtube-embed";

const Kaeforest = evc => {
	/* Add our plugins. */
	evc.addPlugin(syntaxHighlight);
	evc.addPlugin(pluginRss);
	evc.addPlugin(eleventyNavigationPlugin);

	evc.addPlugin(EleventyRenderPlugin);
	// i have fought to get you working
	// evc.addPlugin(eleventyImageTransformPlugin, {
	// 	// output image formats
	// 	formats: ["avif", "jpeg", "png"],
	//
	// 	// output image widths
	// 	widths: ["auto"],
	//
	// 	// optional, attributes assigned on <img> nodes override these values
	// 	htmlOptions: {
	// 		imgAttributes: {
	// 			loading: "lazy",
	// 			decoding: "async",
	// 		},
	// 		pictureAttributes: {}
	// 	},
	// });
	//
	//   God damn I want youtube embeds now
	evc.addPlugin(embedYouTube);
	evc.addPlugin(directoryOutputPlugin, {
		columns: {
			filesize: true,
			benchmark: true,
		},
		warningFileSize: 400 * 1000,
	});

	// Generate a table of contents u.u
	evc.addPlugin(pluginTOC);

	/* Foambubble wiki links */
	evc.addTransform("wiki-links", (content, outputPath) => {
		if (outputPath?.endsWith(".html")) {
			// We remove outer brackets from links
			const output = content.replace(/(\[+(\<a(.*?)\<\/a\>)\]+)/g, "$2");
			return output;
		}

		return content;
	});

	// Return the smallest number argument
	evc.addFilter("min", (...numbers) => {
		return Math.min.apply(null, numbers);
	});

	/* Filter for dates with luxon */
	evc.addFilter("asPostDate", (dateObj) =>
		DT.fromJSDate(dateObj).toLocaleString(DT.DATE_MED),
	);

	evc.addFilter("readableDate", (dateObj) => {
		return DT.fromJSDate(dateObj, { zone: "gmt" }).toFormat("dd LLL yyyy");
	});

	// https://html.spec.whatwg.org/multipage/common-microsyntaxes.html#valid-date-string
	evc.addFilter("htmlDateString", (dateObj) => {
		return DT.fromJSDate(dateObj, { zone: "gmt" }).toFormat("yyyy-LL-dd");
	});

	// Get the first `n` elements of a collection.
	evc.addFilter("head", (array, n) => {
		if (!Array.isArray(array) || array.length === 0) {
			return [];
		}
		if (n < 0) {
			return array.slice(n);
		}

		return array.slice(0, n);
	});

	const filterTagList = (tags) => {
		return (tags || []).filter(
			(tag) =>
				["all", "nav", "notes", "pages", "post", "posts"].indexOf(tag) === -1,
		);
		// this may be a surpise line that orders our thigns
		// .sort((a, b) => b.data.order - a.data.order);
	};
	evc.addFilter("filterTagList", filterTagList);

	// order our things by the value in the order key
	function sortByOrder(values) {
		const vals = [...values]; // this *seems* to prevent collection mutation...
		return vals.sort((a, b) => Math.sign(a.data.order - b.data.order));
	}

	evc.addFilter("sortByOrder", sortByOrder);

	/* Create an array of all these tags */
	evc.addCollection("tagList", (collection) => {
		const tagSet = new Set();
		// biome-ignore lint/complexity/noForEach: <explanation>
		collection.getAll().forEach((item) => {
			// biome-ignore lint/complexity/noForEach: <explanation>
			(item.data.tags || []).forEach((tag) => tagSet.add(tag));
		});

		return filterTagList([...tagSet]);
	});

	evc.addFilter("find_repo", (array, url) => {
		const result = array.find((item) => item.source.url === url);
		return result;
	});

	const markdownLibrary = markdownIt({
		html: true,
		breaks: true,
		linkify: true,
		typographer: true,
		replaceLink: (link, env) => {
			const isRelativePattern = /^(?!http|\/).*/;
			const lastSegmentPattern = /[^\/]+(?=\/$|$)/i;
			const isRelative = isRelativePattern.test(link);

			if (isRelative) {
				const hasLastSegment = lastSegmentPattern.exec(env.page.url);
				// If it's nested, replace the last segment
				if (hasLastSegment && env.page.url) {
					return env.page.url.replace(lastSegmentPattern, link);
				}
				// If it's at root, just add the beginning slash
				return env.page.url + link;
			}

			return link;
		},
	})
		.use(markdownItAnchor, {
			permalink: markdownItAnchor.permalink.ariaHidden({
				placement: "after",
				class: "direct-link",
				symbol: "#",
				level: [1, 2, 3, 4],
			}),
			slugify: evc.getFilter("slug"),
		})
		.use((md) => {
			// Recognize Mediawiki links ([[text]])
			md.linkify.add("[[", {
				validate: /^\s?([^\[\]\|\n\r]+)(\|[^\[\]\|\n\r]+)?\s?\]\]/,
				normalize: (match) => {
					const parts = match.raw.slice(2, -2).split("|");
					parts[0] = parts[0].replace(/.(md|markdown)\s?$/i, "");
					match.text = (parts[1] || parts[0]).trim();
					match.url = `/notes/${parts[0].trim()}/`;
				},
			});
		})
		.use(mathjax3)
		.use(markdownItEmoji) // Our deer emojis **must** work!
		.use(markdownItReplaceLink)
		.use(markdownItFootnote)
		.use(markdownItUnderline)
		.use(markdownItMath);

	evc.setLibrary("md", markdownLibrary);
	evc.addFilter("markdown", (content) => markdownLibrary.render(content));
	evc.addPairedShortcode("markdown", (content) => md.render(content));

	evc.addNunjucksFilter(
		"excludeFromCollection",
		(collection=[], pageUrl=this.ctx.page.url) =>
			// find em all
			collection.filter(post => post.url !== pageUrl)
		);

	evc.addFilter(
		"filterByTags",
		(collection=[], ...requiredTags) => {
		return collection.filter(post => 
			requiredTags.flat().every(tag => post.data.tags?.includes(tag))
		);
	});


	// Override Browsersync defaults (used only with --serve)
	evc.setBrowserSyncConfig({
		ui: false,
		ghostMode: false,
		callbacks: {
			ready: (_err, browserSync) => {
				const content_404 = fs.readFileSync("_www/404.html");

				browserSync.addMiddleware("*", (_request, response) => {
					// Provides the 404 content without redirect.
					response.writeHead(404, {
						"Content-Type": "text/html; charset=UTF-8",
					});
					response.write(content_404);
					response.end();
				});
			},
		},
	});

	/* And now our passthru copies! */
	evc.addPassthroughCopy(
		{
			"static/content": "static/content",
			"site/javascript": "site/javascript",
			"static/img": "static/img",
			"static/media": "static/media",
			"static/icons": "static/icons",
			".well-known": ".well-known",
		},
		{
			// debug: true,
			dot: false,
			junk: false,
			filter: ["**/*", "!.DS_Store"],
		},
	);

	return {
		markdownTemplateEngine: "njk",
		pathPrefix: "/",
		dir: {
			input: "site",
			includes: "/_includes",
			data: "/_data",
			output: "_www",
		},
	};
};

export default Kaeforest;