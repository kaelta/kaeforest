---
title: ${1:$TM_FILENAME_BASE}
description: ${2:Description}
foam_template:
  name: Dev post
  description: A note on some dev-related topic.
  filepath: 'site/notes/$FOAM_TITLE.md'
---

${4:Content!}
