---
title: London
description: Shots and figures from our time in London.
date: 2023-11-29
layout: layouts/post.njk
tags: travel
---

London was both a turbulent yet amazing part of our lives, and where I currently still live, as of November 2023.
We were rapidly developing [[gaia]] during this time, growing our skills as artists, and developing a lot of our artwork.
All photos were shot on an iPhone xs max.

## Edgware, North London

My girlfriend and I were staying here a while developing [[Gaia]].
We often travelled out to Central London to do graffiti and meet other artist friends we met along the way.

![Road to the North.](/static/img/IMG_6080.JPG)

![Cat around London](/static/img/London_cat.jpg)

## Lambeth, Central London

Here's where we did the bulk of our art. It became a place of relaxation for us, meeting friends, finding new people and growing our statuses as DJs and music artists.

![London Tower](/static/img/London_tower.jpg)
Elizabeth Tower, Lambeth

![The KAEX tag I've adopted](/static/img/IMG_6035.JPG)

![Kae in Leake Street](/static/img/D28605F9-8312-4231-9B31-E1F03301381A.jpg)

![Barbican Council estate Tower](/static/img/barbican-tower.jpg)

![The harsh brutalism of Farringdon.](/static/img/london-brutalism.jpg)

## Richmond, West London

West London marked the beginning of the end to our era in working for our old company. It was in this time we stayed with a few friends, often going out to enjoy the nature that West London had.
Our friend, a heavy spritualist explained to us the nature of meditation, our places in the world as light bringing entities, and provided a wealth of support to help us make sense of who we were.

![pidgeon :3](/static/img/IMG_6418.JPG)

![Deer!!!](/static/img/IMG_6456.JPG)

![f](/static/content/img/london/rock-macroscale.jpeg)

![Steps](/static/img/IMG_6337.JPG)

![Flowers](/static/img/IMG_6279.JPG)

![Deer in the field,,](/static/img/IMG_6458.JPG)

![Watching the sunset](/static/img/IMG_6327.JPG)
