---
title: Gaia
description: A next generation framework for Web3.
date: 2023-11-29
tags: dev
---

Gaia is a web3 application development toolkit designed for developers to create innovative web3 websites.
It was developed with me and my girlfriend during 2023, after breaking off from our old company that we worked for.

## Inception

The vision of Gaia is to empower creatives, who wish to earn money and advertise their material, with the ability to freely host, distribute and sell their material on their own terms.

It was created as an experiment in digital marketing in which artists would become their own advertisers, producers and agents, to restore a level of control that is currently left to large distribution services such as Spotify, record labels, or even Amazon (in the form of its subsidiaries such as Soundcloud and Amazon Music).

Throughout various industries, we see record labels - who often "shape" or "influence" an artist to become the most profitable at the cost of artistic integrity, or in the fields of academia; research papers are gate-kept by large academic institutions.

This would mean that services such as advertising for brands would be implemented directly within the NFT - the material which they would upload, using systems of stickers, ads and other metadata on the web, as long as the agreement the artist has set is satisfied by the advertising party.

## Tech Stack

The Gaia toolset is written in pure Typescript, leveraging Hardhat and Ethers.JS.

## Try it out!

<https://gitlab.com/gcnet-uk/gaia/gaia>
