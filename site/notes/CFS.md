---
title: CFS
description: Colonial Forces of Saturn
date: 2024-01-06
---

CFS were the main antagonist of [[project-titan]].
Akin to the USA of modern ages, they wished to be an empire that ruled over humanity.
To me, they represent the overwhelming desire to control the natural growth of civilizations.

