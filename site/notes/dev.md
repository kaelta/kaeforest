---
title: Dev
description: Software development is a way of expressing our thoughts & ideas through code.
layout: layouts/wiki.njk
date: 1970-01-01
tags:
- research
---

![computers](/static/content/img/carrie-computers.jpg)
<cite>Decompustruction | 23IC2</cite>

We use software so we can enable ourselves in the real world.
This software is made by developers who (may) have goals which do not fully align with ours; sometimes they may do, and at other times, just occasionaly intersect.

When we ++consume software++, we give data to receive function caused by other actors.
In this realm, it's important for us to understand what we truly desire from such external actors, and the impact they make on our lives.
In a time where we find a divergence from said desires, these softwares may exhibiting behaviour we would call *erroneous*, or in other words, *bugs*, even when they are functions (features) of the program we simply do not understand; ergo, our tools outgrow us.

In this situation, we are left with 3 choices:

1. To try and fix the error (alter the program's behaviour)
2. Try learning a new program
3. Create the program ourselves.

All but option 3 lead us further down the path, and lead us to fixing these issues, eventually so much so we lose sight of our original intention.

As for the developers, our job is to create the program in such a way that it can perform
each of its intended goals, set in its **mission statement.**
We try to write a program we can call "elegant" - defined as being a program that - exists as the smallest, most comprehensive package of features, code and functionality within its domain, with its own ++unique output++.

Programing, in this sense, is an artform, which creates new structures for others of which did not exist, or have existed, but in a more limited scope. It becomes a tool, a platform to enable others to achieve more than what was previously possible.

## The languages of computers are expressions of the human desire.

Below, you can find a curated list of programs, and other notes to programs I have written / will write, including other references to [[azamaet]] thrown in on the occasion, which acts as a creative reference of which I build some software upon.
Lately, I mostly write in `TypeScript` or `Rust` on the odd occasion.


{% for post in collections.dev | filterTagList | reverse %}
  <a href={{post.url}}> {{post.data.title}} </a> - <cite> {{post.data.description}} </cite>
{% endfor %}
