---
title: HyperTalk & the Macintosh System
description: |
  HyperTalk (was) a BASIC-like object-sending scripting language for the OSX system.
date: 2022-09-04
tags: 'dev'
---

The main use we have found for it is creating smol games, such as [[project-titan]].

Devine Lu Linevega also made a short game with it, with instructions you can see [here](https://wiki.xxiivv.com/site/hypertalk.html)
