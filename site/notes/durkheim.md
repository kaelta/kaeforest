---
title: Émile Durkheim
description: Émile Durkheim was a French sociologist who birthed the concept of the collective unconscious.
date: 2023-11-28
tags: ['sociology', 'postmodernism']
---

In his most notable title, _De la division du travail social_, or _"The division of labour in society"_, he described how the social hierarchy was maintained through varied forms of [[solidarity-both-organic-and-mechanical]], and the transition from "primitive" societies to advance the ages of [[civilizations]][^1].
He noted that the effect of the [[collective-unconscious]] has a far more profound effect in a primitive society, where the group at hand must be led by a figure, or worship some Godhead.
These societies are bred to adorn figures of wit, intelligence and leadership, rather than those whom hold a far more capitalistic influence, thus making them stronger and more unified as a group, but far more susceptible to outside memetic forces.

It was by understanding this very premise, it could be argued, colonial ruling powers maintained their iron grip over other tribes and peoples, by creating ideological societies that would bend to the whim of an ever elusive definition of moral values; subsequently do others who wish to find their sense of belonging for their continued survival will flock to.
Not out of love, or belief, but out of necessity.

## Societies of control


[^1]: <https://en.wikipedia.org/wiki/The_Division_of_Labour_in_Society>
