---
title: Giles Deleuze
description: The founding thinker of Body Without Organs, Rhizomes and many post-modernist ideas.
date: 2024-01-05
tags: sociology
---

Deleuze is the founding father behind a lot of post-modernist thought, whose work spread extremely wide within the philosophical and academic community

There's a lot to write about Deleuze but for now I'll link my favorite podcast about him and his works: Acid Horizon, who also hold various discussions about his other contemporaries like Mark Fisher and Michael Foucault

https://www.youtube.com/c/AcidHorizon

https://www.youtube.com/embed/XCeDiUlNu4I
