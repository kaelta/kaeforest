---
title: Some cute JS libraries.
description: Normalising having cute programs. Make your software *sparkle* ✨
date: 2024-09-18
tags: 'dev'
---

cute js libs need to be normalised again.
software doesnt have enough things anymore to make it actually fun or attractive to use,
and when you have to work with software u hate looking at, why look at it at all.

*liek, why do u think i use emojis in a brutalist ass website :butterfly: - u gotta have fun~*

having :sparkles: cute :sparkles: programs also just literally improves yr mental health when yr debugging,
or just doing anything when staring at a computer, not to mention just being actually better for not straining yr eyes 🎀
having to read text that all looks the same! 

if ive now convinced you to help alleviate your depression,
here's a list of a few i love:

### 1. pino
![pino](/static/content/img/pino-banner.png)

[pino](https://github.com/pinojs/pino) is one of those libs u gotta use with its partner, `pino-pretty`, and you get some really *~aesthetic~* output w logs~
if u love staring at pretty & useful colours when debugging lines to help yr headache, try it out :)

### 2. zustand
![zustand](/static/content/img/zustand.jpg)

[zustand](https://zustand.docs.pmnd.rs/getting-started/introduction) is a super powerful state management lib w rly nice & easy functionality & pluggable middleware.
it's not exactly *cute* like... *aesthetic*,, but it certainly makes yr code more beatiful & understandable using it :)

### 3. biome.js
![biome](/static/content/img/biome.svg)

[biome](https://biomejs.dev/) is a very cute prettifier that i found by accident lately~ i remember back in 2019 when we were all using eslint &/or prettier,
eslint having the more difficult and complex setup - react required you to give 10 years just to get a good working setup with babel, webpack and jest, and vercel was still called zeit.

boomer rambles aside, biome feels like prettier and eslint again, but just 100x nicer than both. it'll automagically make yr files *~aesthetic~*, give u explanations 4 code errors and.. yea thats it!
u shld use it!

### 4. pico.css
![pico.css](/static/content/img/picocss.png)

[pico.css](https://picocss.com/) is the best css framework around. why?
you dont even have to write css. need i even say more.

