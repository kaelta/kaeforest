---
title: Guides
description: My wife said I should...
date: 2024-12-07
tags: guides
---

...write guides on the random things I know that she finds useful.

So I'll update this with random tidbits of things I know.

## Best VPN(s)

[mullvad](https://mullvad.net) remains the all time best. Prices start at ~4,19£/mo, and payment is available with cash and crypto.

## Software/Code Deployment

To escape the Vercel and big YAML Conglomerate.

If you want something up for your personal projects but don't need it constantly turned on, check out the free plan on [Render](https://render.com). Your projects can be restarted and ran again simply by visiting the domain it's deployed on again (after a few moments for boot time) so you don't have to embarrass yourself when you have to log in to your dashboard just to show your friend your sick ass website.

> Lol,

..*they mutter*

> ...lmao. this petulant wee lass cannae even keep a website working. the fool. sm broke ahh foolishness 💀💀💀

Alternatively, if you have money, clients, &/or a startup or business you'd like to run but don't want to (understandably) configure your own Linux VPS and pay an extortionate amount for its mere presence, you'll be pleased to know they offer a very enticing plan at **19$/mo**. Perfect for deploying multiple services with no hassle or hidden costs. I've personally used Render for my own client work and personal projects, and thanks to its seamless got integration from my gitlab repository, I never have to worry about CI ever again.

Or if you want more fine grain control of your resources, and are happy to pay the bare minimum cost of what ever resources you use (yes this is a pay as you go service billed monthly , a d now you may have in mind a specific and established client base), I cannot recommend [fly.io](https://fly.io) enough.
Their pricing is actually quite good for the resources you get with them.

With both products, you can deploy and orchestrate your entire infra in the cloud in just mere button clicks with either fly's very fancy UI, or if you prefer a more programmatic approach, render supports *blueprints*, which are like nix flakes for cloud infrastructure but YAML. God that was a lot of 

## VPS

In the case you really want your very own Linux on the cloud-

[Abelo Hosting](https://abelohost.com/) offer very good plans on a budget price, with an enterprise support option for when you want to impress your CTO at work.
They can be installed with pretty much any Linux you want, their uptime is amazing, the servers are powerful, and I would **1000%** use their services again.
I don't recommend their storage plans though, They're kinda wack. Use [Vultr](https://vultr.com) for such a purpose.

## Domains

Now that You've set up your trillion dollar B2B SaaS Edge computed blazing fast AI driven Google killer startup, now you need a fancy new domain for it to go with.

For this I'd recommend [njal.la](https://njal.la) for such a purpose. They have very good end-user privacy, protrecting your personal info from DNS digging et al. Payments are also available through crypto.

## Mail Services

For when the eyes of Google start to appear in your dreams and the walls start closing in, or if you just want a more holistic approach to email *like it was back in the 90s* - [Disroot](https://disroot.org) got you.
They do however require a 2 day waiting period (possibly sooner, unless on weekends), but once you do pass, they offer an email service among many other niceties like nextcloud (open source Google/iCloud - like cloud storage), should that take your fancy.

[Firemail](https://firemail.de) is also quite good and does not have a waiting period, but does also require you to already own an existing email address. Take your pick.

[Resend](https://resend.dev) is a developer-focused email API that requires an existing address to send mail over, and a service I use in almost every single project that requires it. 
It's a perfect, no hassle method with extreme customisability and dead simple documentation to getting emails done. The way it should be.

## Music

Free music in the modern world should be a right.
I thankfully don't have to pay for it, since I use [Spotify Xmanager](https://xmanagerapp.com).

## Messaging

Use Signal.

## Misc

Any paid service you want probably has a cracked app/open source reimplementation. Just Google for it and you'll find plenty for anything you can imagine.
[Revanced](https://revanced.app) is a great and well known YouTube + music crack for example. No more ads on mobile, at last. If You're on android. iPhones can jailbreak or sth idk just don't anger the spirit of Steve jobs (challenge: impossible)
