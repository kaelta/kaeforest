---
title: On Typescript Enums
description: |
  Why Typescript enums kinda suck, actually.
date: 2023-09-05
tags: dev
---

Typescript enums have been a bane of good development in a lot of my cases.
Take the following enum for example:

```ts
enum Result {
  Ok,
  Err
}
```

In reality, the decompiled typescript results in the following:

```js
var Result;
(function (Result) {
  Result[(Result["Ok"] = 0)] = "Ok";
  Result[(Result["Err"] = 1)] = "Err";
})(Result || (Result = {}));
```

There's so much weirdness in this.
Let's try and parse this.

1. With the *`var`* keyword, it attaches a variable called *`Result`* to the global scope[^1]
2. An IIFE is invoked with *`Result`* as a paremeter
3. *`Result`* is given the value of *`Result`* or *`{}`*
4. *`Result`* is given the values of *`Result["Ok"] = 0`*, which would give:

```ts
[
  {"Ok": 0},
  {"Err": 1}
]
```

After invoking IIFE, we now are left with the following:

```js
var Result = {}
Result["Ok"] = 0
Result["Err"] = 1
```

As *`Result`* is always given the ++[`Symbol.Iterator`](<https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var>)++ type, we can loop over *`Result`* like so:

```ts
Object.entries(Result)

// Returns...

{
  Ok: 0,
  0: "Ok",
  Err: 1,
  1: "Err",
}
```

Like, just *why* would you do this. You're just left with a bunch of repeating indexes which are, awkward to navigate when actually using these in code.
It's why I almost never use them and just use a `Dicitionary` in code instead, as this allows you to join and implement keys from other dictionaries, extend your types and are generally just far more powerful.

[^1]: [The `var` keyword attaches each variable declared to a global scope, for this reason, it is not recommended](<https://softwareengineering.stackexchange.com/questions/274342/is-there-any-reason-to-use-the-var-keyword-in-es6>)
