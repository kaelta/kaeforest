---
title: Synthesis & its practical applications
description: |
  Using synthesis for new generative sound design
date: 2024-01-07
tags: 'audio'
---

Synthesis in my case revolves around mostly two methods to generate the sound I want: that being, *additive* and *FM*.
I'll cover both techniques and how they work, and also provide some demos in a future update (I promise).

## Additive Synthesis

Additive synthesis is the method of adding several sine waves together to form a new waveform- hence the term additive - we're adding more texture and timbre to the sound, depending on the waveform(s) we input.

No sound in the real world is composed of a single wave, but rather a collection of many different sound waves, traveling at different speeds, with different amplitudes, pitch, et cetera.
Therefore, technically - through Additive Synthesis, we can recreate any sound possible!

Using a Fourier Transform, you can analyze the different fundamentals and actually find out what makes a waveform what it is! There's different Fourier Analysis tools you can download as VSTs, personally I use the stock Spectrogram in Ableton, but if you use something else, I recommend [Voxengo Span](https://www.voxengo.com/product/span/)

Frequency of timbre within these sounds have their nomenclature identified by the fundamental frequency - the base pitch from which the sound resonates, i.e: a middle C (within contemporary western music) resonates at 262Hz.

## FM Synthesis
