---
title: Nomad
description: The nomad is the traveling star, receiving the blessings from the hermetic & nomadic tribes.
image: /static/img/nomad.JPG
date: 2022-09-29
tags: 'Azamaet'
---

Think like the star of [Polaris](<https://en.wikipedia.org/wiki/Polaris>), with a more primordial force behind it.
The birth of new civilizations.

In his rekindling after traveling the universe for millennia, he gathered the summation of
his newfound knowledge to [[hyperstition]] the self into physical form.

The form laid bare, the humanoid faceless form, 'cept the void in place of its face,
with nothing but its insignia scarred into its visage.

Often being stated as a "forceful corruption of light", it is heralded as either an unjust, divine light-bringer with a fist of iron; the final bearer of the Spear of Apollo, or the antithesis to all justice and life within Azamaet.


