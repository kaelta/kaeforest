---
title: Now
description: |
  Peek inside the author's mind.
layout: layouts/home.njk
tags: nav
order: 4
---

## M02 22 2025

- Living in the North full time
- Released a track on [[LFP]], the new music label
- Released 2 [demo albums](/pages/releases)


## M02 17 2024 -

- Moved to The North
- Scaled down Genesis Computing
- Became employed

## M09 07 2023 -

- Founded Genesis Computing, a blockchain-based digital marketing company
- Started learning Game Dev
- Perfected skills in:
  - Architecture
  - Sales
  - Marketing

## M03 14 2023 -

- Working within Audiocover as a web engineer
- Took a gap year from University

## 2022

- Living in the United Kingdom
- In university, studying music production
- Learning new & experimental languages & writing systems, such as;
  - [[japanese]], learning [[hiragana]].
  - [[elian]], a newfound writing system for the Latin alphabet.
- Working on creating:
  - Experimental music with new [[synthesis]] techniques under the name h. x. r. †
  - [[project-titan]], our magical girl 2D game in the style of old 90s dungeon crawlers.
    - Perfecting hand-drawn & digital art.

This is a [/now] page, a concept created by Derek Sivers.
It’s a short summary of what we’re doing now, updated as regularly as we can remember to do so.
