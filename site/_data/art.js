export default async () => {
	try {
		const url = "https://api.are.na/v2/channels/artwork-8qh1p1_74oa/contents";
		const art = await fetch(url).then((res) => res.json());
		console.log(art.contents);
		return art.contents;
	} catch (error) {
		if (error) {
			return [
				{
					title: "Theres an error!",
				},
			];
		}
	}
};