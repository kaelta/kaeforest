---
title: Chaotic Sound Synthesis
description: Chaotic Sound Synthesis is sound generated from the modulation of frequencies using fractal equations.
date: 2023-11-28
tags: audio
---

Music has always had the ability to be generated from images - take FL Studio's image resynthesis as an example of this - using the artifacts from a jpeg or png file as modulators in a synthesiser to generate a completely new and original sound.
This technique has been applied in genres from psychedelic ambient, such as ++[Llydia Lancaster's musical work](<https://rreusser.github.io/ueda-attractor/>)++.
These has often produced sounds with very, incomprehensible and novel qualities, with results often then being heavily edited in post to produce the feeling the artist desires.
Using models such as the [[ueda-attractor]], we can genearate a new type of sound with far more variables we can control, by inputting different variables into its equation.

<style>
iframe {
    display: inline-block;
    aspect-ratio: 16 / 9;
}
</style>

<iframe src="https://rreusser.github.io/ueda-attractor/"></iframe>

###### Figure 1.0: A representation of the Ueda Attractor in action. Taken from: <https://rreusser.github.io/ueda-attractor/>

## Attractor models in FM synthesis

> The chaotic FM algorithm includes conventional FM synthesis as a subset. This chaotic FM method can be combined with other chaotic synthesis methods, such as the Ueda attractor, providing an even wider range of sounds. Chaotic synthesis and filtering can be used in both the analog and digital domains, providing similar results.[^1]


[^1]: ../../../static/media/pdf <https://libgen.rs/scimag/10.2307%2F3680960>
[^2]: <https://osf.io/bsyq9/download>
[^3]: <https://www.goodreads.com/book/show/3135391-the-road-to-chaos-theory>
[^4]: <https://researchmap.jp/read0012729>
