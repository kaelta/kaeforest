---
title: Hacking the human likeness mechanism
description: How to get anyone to do anything, responsibly.
date: 2024-01-03
tags: ['sociology']
---

The human condition is probably the most difficult thing to crack, even harder for those with ADHD, autism et al. It is also something I myself struggled with for the longest time, until learning how exactly the human brain worked.
It works on a lot of the same principles mentioned within my writeup on [Cultures Sans Frontiéres](/posts/Culture-Sans-Frontiéres/), a cybernetic open system in which human souls responding to external stimuli will; either converge in defense, become reciprocal to a positive influence or violently defend an external threat. In this form, we can refer to Stafford Beer's lectures on human cybernetics, which I will include in this reference[^1]

Such reactions are deeply ingrained within the human condition and are near immutable traits, defaulted to in order to prolong the survivability of the organism. In every human interaction, a person sends subconscious signals which have the possibility of activating certain (defense) mechanisms of the psyche, and overriding such a mechanism is not an overnight process - nor can it be done with everyone. If you wish to successfully make yourself known as safe to their influence, you must trigger absolutely none of these defense. And once you do, you've now firmly established yourself as an influence over that person, therefore allowing yourself to "backdoor" their psyche.

## Foundation

> Dark skies shrouded the husk of the now lifeless exoplanet. In the wake of the Mother's fury, lie the ruins of great peoples, awesome creatures, decrepit ruins of long gone civilizations, and vacant vestiges of the virile violent tetrarch of their *morte* devotees.
> Emerging from creeping shadows, was a widow, cradling the hollow vessel of her child in her arms. The stillness of her birth, echoing the stagnation of the world it was wrought into.
> The mother cradles it. Her warm embrace falls upon a stone. Like rain, the un-gratifying descent is met only with the same, ecstatic silence.

![Angel's Egg](/static/content/img/angels-egg-1.jpeg)

## Foundation and Empire

## Empire

## Second Foundation

[^1]: *Stafford Beer*, in his Stanford lectures on cybernetics, and the will of the people. <https://www.youtube.com/watch?v=2ybjOw6UJ8A>
[^2]: *Discourse*, by Devine. <https://wiki.xxiivv.com/site/discourse.html>
