---
title: Ethics
description: Ethical Dilemas
layout: layouts/wiki.njk
order: 5
tags: ['writeup', 'sociology', 'postmodernism']
---

![Foucault](/static/content/img/miachael-foucault.webp)

## Ushering forth, in comes the new generation.

When we think to the term of *"collective"*, we'd normally think to something of a squallor of actors, working towards an aligned goal, or to uphold some belief.
To pinpoint the origin of what a collective is, we'll use the notion of """collective""" on the basis principled by [[durkheim]]; ergo, we need only to look at their dogma, see what they decry, what they cherish and what they desire. This could be anything; from wishing to build a small village, creating art, or making an effect that they believe is larger than themselves. Each collective can be said to be an *antithesis*, to a current event or circumstance within their *perceived* natural environment.
And once we identify why, such a collective exists, we can then begin to analyse their perspective - what ideas do they bring? What do they believe to be of a disturbance to their desired environment? And most importantly, how do they wish to go out and create change?

Some might be for example, an advocate for the protection of a species, or plant, or genome or whatever force of natural order from extinction - whether it brings them benefit; to themselves, or to others; and others may be for the annihilation of some other actor, that threatens the order of what they have.
We can then start to derive what may be truth, and what is not a given truth, by the words of the collective, and a rational, non-partisan perspective on what effect they may be describing. If the two match, we can call them a ++"rational"++ group.

Then, let's go on to say that this group wishes for something such as, the aforementioned annihilation of a particular type of species - say, some hyenas that have a fondness for their livestock, and we can measure whether the cause [of their ideals] matches the effect, that they wish to make. If both of these are an equal response, we can say this group is ++"moderate"++. If they are disproportionate in one more, say particularly violent, or aggressively-natured way, we would call them ++"radical"++.

## When a basis for truth can be discerned, we are granted perspective.

Ethics is the basis upon which we uphold an ideal of being ++humane++. By the rationalisations of ethics unto itself, we can discern that being ++humane++ is a strong guiding dogma. Understanding this principle, we can use this practice to logically come to the most ethical, and beneficial solution to any strife, faced within, or outwards.

<div class="flex gap-2">
	<img 
		float="left"
		style="border-radius: 220px; max-height: 128px;"
		src="/static/content/img/al-ghazali-headshot.jpg"
	/>
	<p>
		{% renderTemplate "md" %}
> “First fight thirteen enemies you cannot see - egoism, arrogance, conceit, selfishness, greed, lust, intolerance, anger, lying, cheating, gossiping and slandering. If you can master and destroy them, then you will be ready to fight the enemy you can see.” - Al-Ghazali
		{% endrenderTemplate %}
	</p>
</div>

Fighting these enemies within the self, are the most important before coming to usurp any idealogy, perspective, or otherwise. This will shape who we become as individuals, and the type of effect we should have on the world. Any clouding of judgement, some strata leading us astray from this critique of thinking, and we find ourselves just as lost as our enemy, whomever it may be.


![Al-Ghazali](/static/content/img/al-ghazali.jpeg)

Conquering the emotion of anger may be the most difficult. We can see countless examples in our day-to-day lives where anger is used as a tool; to spread ideals of collectives better rationalised as *radical*.
These *radical* ideas we can say, will use anger as it's an extremely good motivator, for creating immediate ++effect++. And when we begin to discern the source of the anger, and then rationalise its place within the perspective of where it exists, the anger then, becomes information. And thereby using this information, we can act accordingly.

We can use the example of say, a news article that paints a portrait of a *leader*, whom decries a nation for its hostilities to another, and is now threatenening its sanctity.
We have discerned:

- The leader is against harm to a specific collective. *this is humane*.
- A foreign actor is causing harm to a collective. *this is rational*
- To prevent further harm, a threat punishment is introduced to the collective. *this is ++moderate++*

The astute one will identify the source of anger at the *inhumanity* that plagues the given artice, and will therefore, be likely to agree with the views of the leader in this given circumstance, as it exhibits the same ethical view of the individual.

## Strata of deceit will precurse any rhizome, eliminating the prenotion of the self.

And this is where, ultimately, where many traps can lie. If we have applied this judgement to this context without the final step, of applying the subject's perspective to the rest of its environment - in this case, geopolitically, we risk our judgement being turned on us for the approval of another; i.e this leader; whom may have an ulterior motivator which we did not account, leading us further astray from our ethics than what we had previously standardised.

The leader in this case, has significant financial interest in the geopolitical region, and could see this nation as an opportunity for investment. In ceasing its hostilities to its invaders, the leader now would have access to buy up the land of said nation, adding it to its repitoire of economic resources.

This lack of information that was present in the news article however, is what has now shaped the perspective of its reader. It has created an image, leaving omission of its environment; an art piece, vacant of its background or other details that would make it a piece of art.
I really do believe it's valuable to look at news as art for this reason; we can see what "picture" it wants to "paint" for us, but the burden of context is then left to the consumer to rationalise, or even worse, *imagine.*

And the imagining, is *really* the absolute *worst* outcome, yet is easily found prevalent within the world. The *imagining* of context clouds us from the source of context, and too many strata that exists, that is just these figments of imagination, is when rhizomatic truths begin to die out, surrounded by an abundance of "context"; becoming **content**. This, really is,the **greatest evil** there is in our modern society.
When we find ourselves surrounded by **content** we have now lost all **context** which previously existed, truth is now more fable than it is real, until we find our perspective now a fantasy.

![quote](/static/content/img/london/quote.jpg)

## Loss of thought, loss of information, loss of mind, of matter.

Solutions to this issue have been proposed, from varying outlets, thought leaders, corporations et al, all promising to deliver the most "complete" view, but even this in most cases, does not work. In an age where the *digital* and the *real* have become so blurred, truth now begets some motivator: financial, political or otherwise (yet mostly financial), leading its consumers at a far greater loss than one would be without having been a disciple to these collectives, ergo creating bubbles of [[collective-unconscious]].

So this really does beget the question of: who to question, who to not?
What do we consume? Do we *starve?*

<h2 style="font-size:6rem; color:red;">LOOK WITHIN.</h2>



