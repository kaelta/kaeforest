---
title: Home
layout: layouts/home.njk
eleventyExcludeFromNavigation: true
---

## Meta
Welcome to *Kaeforest*, a wiki engine for notes on [[audio]], software [[dev]], and [[language]].

I also post [art](/pages/art) on my [Are.na](https://are.na/kae-elster)

This site aims to be a knowledge base & general collection of [[research]]  which may help (mostly myself) and others record knowledge that may become useful later.
In order to keep a record of my thoughts, my identity and who I am as a person, I write this in a form of meditation to remember who I am, and what I do in life.

## A few notes

##### See all notes [here.](/tags/)

{% for entry in collections.notes | filterTagList | reverse | head(5) %}
  { {{entry.data.date | readableDate}} } - <a href={{entry.url}}> {{entry.data.title}} </a>
{% endfor %}

## Contact

Get in touch with me [using this un-fancy contact page.](/pages/me)

